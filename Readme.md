# KdV Hamiltonians

Build and run

```
 dnu restore
 dnu build
 dnx . run
```

To get started go to https://github.com/aspnet/home


Example output

````
Computing KdV Hiearchy up to order 5
H_{-1} &= \frac12\int - 1q\\
H_{0} &= \frac12\int q^2\\
H_{1} &= \frac12\int 2q^3 + q_x^2\\
H_{2} &= \frac12\int 5q^4 + 10qq_x^2 + q_2^2\\
H_{3} &= \frac12\int 14q^5 + 70q^2q_x^2 + 14qq_2^2 - 252q_x^2q_2 + q_3^2\\
H_{4} &= \frac12\int 42q^6 + 1105q_x^4 + 420q^3q_x^2 - 1262q_2^3 + 11376qq_x^2q_2 + 126q^2q_2^2 - 504q_xq_2q_3 + 1998qq_3^2 + q_4^2\\
H_{5} &= \frac12\int 132q^7 + 24310qq_x^4 + 2310q^4q_x^2 - 135366q_x^2q_2^2 - 27764qq_2^3 + 125136q^2q_x^2q_2 + 924q^3q_2^2 - 3554q_2q_3^2 - 61776qq_xq_2q_3 + 21978q^2q_3^2 + 46108q_xq_3q_4 - 3938qq_4^2 + q_5^2
```