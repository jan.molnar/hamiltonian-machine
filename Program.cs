using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace HamiltonianMachine
{
    public class Program
    {
        public static void Main(string[] args)
        {
            bool printSPolynomials = false;
            int numberOfHamiltonians = 5;

            Console.WriteLine("Computing KdV Hiearchy up to order {0}", numberOfHamiltonians);

            // compute the kdv hierarchy
            // determine the intermediate s polynomails
            var s = KdVSequence(2 * numberOfHamiltonians + 3);
            int sc = 0;

            foreach (var item in s)
            {
                if (printSPolynomials) // print intermediate s polynomials
                {
                    Console.WriteLine("s{0}", sc);
                    item.Print();
                }

                if (sc % 2 == 1) // if sc is of the form 2n+3 print the Hamiltonian
                {                // note that the other Hamiltonians are full derivatives

                    int n = (sc - 3) / 2;

                    Console.Write(@"H_{{{0}}} &= \frac12\int", n);

                    // Remove full derivative terms, simply, and sort

                    var i = item.KillFullDerivatives()
                                .PartiallyIntegrateEachTerm()
                                .Simplify()
                                .OrderBy(p => p.Count)
                                .ToList();

                    foreach (var m in i)
                    {
                        // the even Hamiltonians are multiplied by -1
                        if (n % 2 == 1)
                        {
                            m.Coefficient = -m.Coefficient;
                        }
                    }

                    i.Print();
                }

                sc++;
            }
        }


        public static List<List<Monomial>> KdVSequence(int max)
        {
            var s = new List<List<Monomial>>();

            s.Add(new List<Monomial> { new Monomial() });
            s.Add(new List<Monomial> { new Monomial { 1 } });

            s[1][0].Coefficient = -1;

            for (int n = 2; n <= max; n++)
            {
                var ds = s[n - 1].Derivate();

                for (int m = 0; m <= n - 1; m++)
                {
                    var ss = s[n - 1 - m].MultiplyBy(s[m]);

                    ds = ds.Concat(ss).ToList();
                }

                s.Add(ds.Simplify());
            }

            return s;
        }

    }

    public static class MathTools
    {
        public static void Print(this List<Monomial> input)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder latex = new StringBuilder();

            bool first = true;

            foreach (var monomial in input)
            {
                latex.Append(" ");

                if (monomial.Coefficient < 0)
                {
                    latex.Append("- ");
                }
                else if (!first)
                {
                    latex.Append("+ ");
                }

                if (monomial.Coefficient != 1)
                    latex.AppendFormat("{0}", Math.Abs(monomial.Coefficient));

                for (var i = 0; i < monomial.Count; i++)
                {
                    var exponent = monomial[i];

                    if (exponent == 0)
                        continue;

                    latex.Append("q");

                    switch (i)
                    {
                        case 0:
                            break;
                        case 1:
                            latex.Append("_x");
                            break;
                        default:
                            latex.AppendFormat(@"_{0}", i);
                            break;
                    }

                    if (exponent > 1)
                    {
                        latex.AppendFormat("^{0}", exponent);
                    }

                }


                sb.Append("{");
                sb.AppendFormat("{0}x ", monomial.Coefficient);
                sb.Append(String.Join(", ", monomial.Select(i => i.ToString()).ToArray()));
                sb.Append("}");

                int w1 = 0;

                for (int i = 0; i < monomial.Count; i++)
                {
                    w1 += (2 + i) * monomial[i];
                }

                w1 = w1 / 2;

                sb.AppendFormat(" {0}\n", w1);
                first = false;
            }

            latex.Append(@"\\");

            //Console.WriteLine(sb.ToString());
            Console.WriteLine(latex.ToString());
        }

        public static List<Monomial> MultiplyBy(this List<Monomial> input1, List<Monomial> input2)
        {
            var output = new List<Monomial>();

            foreach (var monomial1 in input1)
            {
                foreach (var monomial2 in input2)
                {
                    List<int> x;
                    List<int> y;

                    if (monomial1.Count == 0 | monomial2.Count == 0)
                        continue;

                    if (monomial1.Count > monomial2.Count)
                    {
                        x = monomial1.ToList();
                        y = monomial2;
                    }
                    else
                    {
                        x = monomial2.ToList();
                        y = monomial1;
                    }

                    for (int i = 0; i < y.Count; i++)
                    {
                        x[i] += y[i];
                    }

                    output.Add(new Monomial(x) { Coefficient = monomial1.Coefficient * monomial2.Coefficient });
                }
            }


            return output;
        }

        public static List<Monomial> Derivate(this List<Monomial> input)
        {
            var output = new List<Monomial>();

            foreach (var monomial in input)
            {
                for (var i = 0; i < monomial.Count; i++)
                {
                    if (monomial[i] == 0)
                        continue;

                    var z = new Monomial(monomial);

                    z.Coefficient = monomial.Coefficient * z[i];

                    z[i]--;

                    if (monomial.Count > i + 1)
                        z[i + 1]++;
                    else
                        z.Add(1);

                    output.Add(z);
                }
            }

            return output;
        }

        public static List<Monomial> Simplify(this List<Monomial> input)
        {
            var output = new List<Monomial>();

            // group monomials by exponents
            var q = from m in input
                    group m by m.Key into g
                    select g;

            foreach (var g in q)
            {
                var m = new Monomial(g.First())
                {
                    Coefficient = g.Sum(z => z.Coefficient)
                };

                output.Add(m);
            }

            return output;
        }

        public static List<Monomial> PartiallyIntegrate(this Monomial monomial)
        {
            // MonomialIntegrable ensures that
            // a) n = monomial.Count > 2
            // b) m[n-1] = 1
            // c) m[n-2] = 0

            var output = new List<Monomial>();

            int n = monomial.Count;

            var m1 = new Monomial(new int[n - 1]);

            m1.Coefficient = -1;
            m1[n - 2] = 1;

            var m2 = new Monomial(monomial);

            m2.Coefficient = monomial.Coefficient;
            m2.RemoveAt(n - 1);
            m2.RemoveAt(n - 2);

            var d = (new List<Monomial>() { m2 }).Derivate();

            output = d.MultiplyBy(new List<Monomial>() { m1 });

            return output;
        }

        // partially integrate each monomial of the given polynomial
        // for which partial integration makes sense.
        public static List<Monomial> PartiallyIntegrateEachTerm(this List<Monomial> input)
        {
            var output = new List<Monomial>();

            int k = 0;

            foreach (var monomial in input)
            {
                if (monomial.IsIntegrable())
                {
                    //Console.WriteLine("Integrate {0}", k);

                    output.AddRange(monomial.PartiallyIntegrate().PartiallyIntegrateEachTerm());
                }
                else
                {
                    output.Add(monomial);
                }
                k++;
            }

            return output;
        }

        public static bool IsIntegrable(this Monomial monomial)
        {
            return monomial.Count > 2 && (monomial[monomial.Count - 1] == 1 & monomial[monomial.Count - 2] == 0);
        }

        public static List<Monomial> KillFullDerivatives(this List<Monomial> input)
        {
            var output = new List<Monomial>();

            foreach (var monomial in input)
            {
                bool flag1 = false;

                for (var i = 0; i < monomial.Count; i++)
                {
                    if (monomial[i] == 0)
                        continue;

                    if (!flag1)
                    {
                        if (i == monomial.Count - 1)
                        { // at the end
                            if (i == 0 | monomial[i] != 1) // no der or higher power
                                output.Add(monomial);
                        }
                        else if (monomial[i + 1] == 1)
                        {
                            i++;
                        }

                        flag1 = true;
                    }
                    else
                    {
                        output.Add(monomial);
                    }
                }
            }


            return output;

        }

    }


    public class Monomial : List<int>
    {
        public Monomial() : base()
        {

        }

        public Monomial(IEnumerable<int> values) : base(values)
        {

        }

        public decimal Coefficient { get; set; }

        public string Key { get { return string.Join(",", ToArray()); } }
    }
}
